//
//  ClassAppDelegate.h
//  Timer
//
//  Created by Jumpei Kondo on 2012/09/28.
//  Copyright (c) 2012年 Jumpei.K. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
