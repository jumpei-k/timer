//
//  ClassViewController.h
//  Timer
//
//  Created by Jumpei Kondo on 2012/09/28.
//  Copyright (c) 2012年 Jumpei.K. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassViewController : UIViewController{
}

@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UILabel *label;


//-(void)onTimer:(NSTimer*)timer;

- (IBAction)startAction:(id)sender;
- (IBAction)resetAction:(id)sender;

@end
