# Timer #

## Step ##
1. When start button is tapped, every X seconds, updateTimer() is called.
2. updateTimer() increases a lapsedTime variables by X, creating a new NSData object, converting it into NSString, and show label

## Code ##
``` objective-C
- (void)viewDidLoad
{
    [super viewDidLoad];
    lapsedTime = 0.0;
    timeInterval = 0.1;
    self.label.text = @"00:00:00:00";
    isFired = FALSE;
    isPaused = FALSE;
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss.SS"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)startAction:(id)sender {
    if (!isFired) { // Start
        // Called one time
        NSDate *now = [NSDate date];
        NSLog(@"now: %@", now);
        timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        [_startBtn setTitle: @"Pause" forState: UIControlStateNormal];
        isFired = TRUE;
    } else {
        // Pause
        if (!isPaused) {
            [timer invalidate];
        [_startBtn setTitle: @"Resume" forState: UIControlStateNormal];
            isPaused = TRUE;
        } else { // Resume
            timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
            NSLog(@"Resume!");
        [_startBtn setTitle: @"Pause" forState: UIControlStateNormal];
            isPaused = FALSE;
        }

    }
}


- (void)updateTimer
{
    lapsedTime += timeInterval;
    NSDate *DateForInterval = [NSDate dateWithTimeIntervalSince1970:lapsedTime];
//    // Format the elapsed time and set it to the label
    NSString *timeString = [dateFormatter stringFromDate:DateForInterval];
    self.label.text = timeString;
}


- (IBAction)resetAction:(id)sender {
    [timer invalidate];

    lapsedTime = 0;
    isFired = FALSE;
    isPaused = FALSE;

    NSDate *DateForInterval = [NSDate dateWithTimeIntervalSince1970:lapsedTime];

    NSString *timeString = [dateFormatter stringFromDate:DateForInterval];
    self.label.text = timeString;
    [_startBtn setTitle: @"Start" forState: UIControlStateNormal];
    isPaused = FALSE;
    
}
@end
```