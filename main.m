//
//  main.m
//  Timer
//
//  Created by Jumpei Kondo on 2012/09/28.
//  Copyright (c) 2012年 Jumpei.K. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ClassAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ClassAppDelegate class]));
    }
}
